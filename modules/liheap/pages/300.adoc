= 300 Detect and Prevent Fraud and Abuse
:chapter-number: 300
:effective-date: October 2020
:policy-number: 300
:policy-title: Detect and Prevent Fraud and Abuse
:previous-policy-number:

include::partial$policy-header.adoc[]

== Overview and Requirements

As a grant recipient, CAAs must protect the program and the source of your federal funds by detecting and preventing fraud

. Establish an adequate and effective system of accounting, internal controls, records control, and records retention.
. Implement an internal compliance and ethics program that encourages the recognition and reporting of fraud, waste, or abuse.
. Report suspected fraud to the DHS Office of Inspector General of the State of Georgia:

=== Office of Inspector General

The DHS Office of Inspector General provides oversight to ensure the work of DHS is conducted according to state and federal laws, and administrative policy, procedure, and practice.
Our goal is to actively seek to eliminate poor management practices, fraud, waste, and abuse within DHS programs and to uncover criminal conduct by employees, contractors, or recipients of public assistance benefits.
These services are provided through five distinct units: Background Investigations Unit, Internal Audits Unit, Benefits Recovery Unit, Internal Investigations Unit, and the Residential Child Care Unit.

*HOW TO REPORT FRAUD, WASTE OR ABUSE*:
[%hardbreaks]
OIG Hotline: 1-844-694-2347
OIG Fax: 404-463-5496
OIG Email: inspectorgeneralhotline@dhr.state.ga.us
Via the Web: https://dhs.georgia.gov/dhs-oig-incident-form[online form]
U.S. Mail: Two Peachtree St., NW, Suite 30.450,
Atlanta, GA 30303
Attention: DHS Inspector General
https://dhs.georgia.gov/dhs-oig-incident-form[DHS OIG Incident Form]
https://dhs.georgia.gov/residential-child-care[DHS OIG Residential Child Care Unit]
